/*************************************************************************
 *  Copyright (C) 2020 by Jean Lima Andrade <jeno.andrade@gmail.com>     *
 *                                                                       *
 *  This program is free software; you can redistribute it and/or        *
 *  modify it under the terms of the GNU General Public License as       *
 *  published by the Free Software Foundation; either version 3 of       *
 *  the License, or (at your option) any later version.                  *
 *                                                                       *
 *  This program is distributed in the hope that it will be useful,      *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 *  GNU General Public License for more details.                         *
 *                                                                       *
 *  You should have received a copy of the GNU General Public License    *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.*
 *************************************************************************/

#include "core/markedclass.h"
#include "core/serializer.h"
#include "image/polygon.h"
#include "text/sentence.h"
#include "util/fileutils.h"

#include <QDir>
#include <QFile>
#include <QHash>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QtGlobal>
#include <QXmlStreamWriter>
#include <video/frame.h>

QVector<MarkedObject*> Serializer::read(const QString& filepath)
{
    QVector<MarkedObject*> objects;

    if (QFile::exists(filepath)) {
        if (filepath.endsWith(".xml"))
            objects = readXML(filepath);
        else if (filepath.endsWith(".json"))
            objects = readJSON(filepath);
    }

    return objects;
}

QString Serializer::serialize(const QVector<MarkedObject*>& objects, OutputType outputType)
{
    if (outputType == OutputType::XML)
        return toXML(objects);

    else if (outputType == OutputType::JSON)
        return toJSON(objects);

    return QString();
}

QString Serializer::toXML(const QVector<MarkedObject*>& objects)
{
    if (objects.isEmpty())
        return QString();

    QString xmldoc;

    QXmlStreamWriter xmlWriter(&xmldoc);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();
    xmlWriter.writeStartElement("annotation");

    auto writeXMLObject = [&](const QString& unitName, int x, int y) {
        xmlWriter.writeStartElement(unitName);

        xmlWriter.writeStartElement("x");
        xmlWriter.writeCharacters(QString::number(x));
        xmlWriter.writeEndElement();

        xmlWriter.writeStartElement("y");
        xmlWriter.writeCharacters(QString::number(y));
        xmlWriter.writeEndElement();

        xmlWriter.writeEndElement();
    };

    for (MarkedObject* object : objects) {
        xmlWriter.writeStartElement("object");

        if (object->type() == MarkedObject::Type::Sentence) {
            xmlWriter.writeStartElement("class");
            xmlWriter.writeCharacters(object->objClass()->name());
            xmlWriter.writeEndElement();

            const Sentence *sentence = static_cast<const Sentence*>(object);
            writeXMLObject(sentence->unitName(), sentence->begin(), sentence->end());
        }
        else if (object->type() == MarkedObject::Type::Polygon) {
            xmlWriter.writeStartElement("class");
            xmlWriter.writeCharacters(object->objClass()->name());
            xmlWriter.writeEndElement();

            const Polygon *polygon = static_cast<const Polygon*>(object);
            xmlWriter.writeStartElement("Polygon");
            for (const QPointF& point : *polygon)
                writeXMLObject(polygon->unitName(), point.x(), point.y());
            xmlWriter.writeEndElement();
        }
        else if (object->type() == MarkedObject::Type::Frame) {
            Frame *frame = static_cast<Frame*>(object);
            if (frame->polygons().isEmpty())
                continue;
            xmlWriter.writeStartElement("class");
            xmlWriter.writeCharacters(object->objClass()->name());
            xmlWriter.writeEndElement();

            xmlWriter.writeStartElement("fr");
            xmlWriter.writeAttribute("id", QString::number(frame->frameId()));
            for (auto polygon : frame->polygons()) {
                xmlWriter.writeStartElement("Polygon");
                xmlWriter.writeAttribute("class", object->objClass()->name());
                for (const QPointF& point : *polygon)
                    writeXMLObject(polygon->unitName(), point.x(), point.y());
                xmlWriter.writeEndElement();
            }
            xmlWriter.writeEndElement();
        }

        xmlWriter.writeEndElement();
    }

    xmlWriter.writeEndElement();

    xmlWriter.writeEndElement();
    xmlWriter.writeEndDocument();

    return xmldoc;
}

QString Serializer::toJSON(const QVector<MarkedObject*>& objects)
{
    if (objects.isEmpty())
        return QString();

    QJsonArray classesArray;

    auto createUnitObject = [](int x, int y) {
        QJsonObject unitObject;
        unitObject.insert("x", QString::number(x));
        unitObject.insert("y", QString::number(y));
        return unitObject;
    };

    for (MarkedObject* object : objects) {
        QJsonObject recordObject;

        if (object->type() == MarkedObject::Type::Sentence) {
            recordObject.insert("Class", object->objClass()->name());
            const Sentence *sentence = static_cast<const Sentence*>(object);
            QJsonObject unitObject = createUnitObject(sentence->begin(), sentence->end());
            recordObject.insert(object->unitName(), unitObject);
        }
        else if (object->type() == MarkedObject::Type::Polygon) {
            recordObject.insert("Class", object->objClass()->name());
            QJsonArray objectsArray;

            const Polygon *polygon = static_cast<const Polygon*>(object);
            for (const QPointF& point : *polygon) {
                QJsonObject unitObject = createUnitObject(point.x(), point.y());
                QJsonObject markedObject;
                markedObject.insert(object->unitName(), unitObject);
                objectsArray.push_back(markedObject);
            }

            recordObject.insert("Polygon", objectsArray);
        }
        else if (object->type() == MarkedObject::Type::Frame) {
            Frame *frame = dynamic_cast<Frame*>(object);
            if (frame->polygons().isEmpty())
                continue;
            recordObject.insert("Class", object->objClass()->name()); // this shouldn't be used, it's here just for compatibility
            recordObject.insert("id", frame->frameId());

            QJsonArray polygonsArray;

            for (auto polygon : frame->polygons()) {
                QJsonObject classObject;
                classObject.insert("Class", polygon->objClass()->name());

                QJsonArray pointsArray;
                for (const QPointF& point : *polygon){
                    QJsonObject unitObject = createUnitObject(point.x(), point.y());
                    QJsonObject markedObject;
                    markedObject.insert(polygon->unitName(), unitObject);
                    pointsArray.push_back(markedObject);
                }
                classObject.insert("Polygon", pointsArray);
                polygonsArray.push_back(classObject);
            }
            recordObject.insert("Frame", polygonsArray);
        }
        classesArray.push_back(recordObject);
    }

    return QJsonDocument(classesArray).toJson();
}

QVector<MarkedObject*> Serializer::readJSON(const QString& filepath)
{
    QVector<MarkedObject*> savedObjects;
    QHash<QString, MarkedClass*> markedClasses;

    QByteArray data = getData(filepath);
    QJsonDocument doc = QJsonDocument::fromJson(data);

    QJsonArray objectArray = doc.array();

    for (const QJsonValue& jsonObj : qAsConst(objectArray)) {
        QString className = jsonObj["Class"].toString();

        MarkedObject::Type objectType;
        if (jsonObj["st"] != QJsonValue::Undefined)
            objectType = MarkedObject::Type::Sentence;
        else if (jsonObj["Polygon"] != QJsonValue::Undefined)
            objectType = MarkedObject::Type::Polygon;
        else if (jsonObj["Frame"] != QJsonValue::Undefined)
            objectType = MarkedObject::Type::Frame;

        if (!markedClasses.contains(className))
            markedClasses[className] = new MarkedClass(className);

        MarkedObject* object = nullptr;
        if (objectType == MarkedObject::Type::Sentence) {
            QJsonObject sentenceObj = jsonObj["st"].toObject();
            object = new Sentence(markedClasses[className], sentenceObj["x"].toString().toDouble(), sentenceObj["y"].toString().toDouble());
        }
        else if (objectType == MarkedObject::Type::Polygon) {
            QVector<QPointF> polygonPoints;
            for (const QJsonValue& unitObj : jsonObj["Polygon"].toArray()) {
                QJsonObject pointJson = unitObj["pt"].toObject();
                QPointF pointObj(pointJson["x"].toString().toDouble(), pointJson["y"].toString().toDouble());
                polygonPoints << pointObj;
            }
            object = new Polygon(markedClasses[className], polygonPoints);
        }
        else if (objectType == MarkedObject::Type::Frame) {
            QVector<Polygon*> polygons;

            for(auto polygon : jsonObj["Frame"].toArray()) {
                QString cName = polygon.toObject()["Class"].toString();
                if (!markedClasses.contains(cName))
                    markedClasses[cName] = new MarkedClass(cName);

                QVector<QPointF> polygonPoints;
                for (const QJsonValue& unitObj : polygon.toObject()["Polygon"].toArray()) {
                    QJsonObject pointJson = unitObj["pt"].toObject();
                    QPointF pointObj(pointJson["x"].toString().toDouble(), pointJson["y"].toString().toDouble());
                    polygonPoints << pointObj;
                }
                polygons.push_back(new Polygon(markedClasses[cName], polygonPoints));
            }
            qint64 frame = jsonObj["id"].toInt();

            object = new Frame(markedClasses[className], frame, polygons);
        }

        if (object != nullptr)
            savedObjects << object;
    }

    return savedObjects;
}

QVector<MarkedObject*> Serializer::readXML(const QString& filepath)
{
    QVector<MarkedObject*> savedObjects;
    QHash<QString, MarkedClass*> markedClasses;

    QByteArray data = getData(filepath);

    QXmlStreamReader xmlReader(data);
    xmlReader.readNextStartElement();

    auto readNextXY = [&]() {
        xmlReader.readNextStartElement();
        double x = xmlReader.readElementText().toDouble();
        xmlReader.readNextStartElement();
        double y = xmlReader.readElementText().toDouble();
        xmlReader.skipCurrentElement();

        return QPair<int, int>(x, y);
    };

    while (!xmlReader.atEnd()) {
        QXmlStreamReader::TokenType token = xmlReader.readNext();
        if (token == QXmlStreamReader::StartElement) {
            xmlReader.readNextStartElement();

            QString className = xmlReader.readElementText();

            if (!markedClasses.contains(className))
                markedClasses[className] = new MarkedClass(className);

            xmlReader.readNextStartElement();

            MarkedObject* object;
            if (xmlReader.name() == "Polygon") {
                QVector<QPointF> polygonPoints;

                while (xmlReader.name() != "object") {
                    if (xmlReader.name() == "pt") {
                        QPair<int, int> pointXY = readNextXY();
                        polygonPoints << QPointF(pointXY.first, pointXY.second);
                    }
                    xmlReader.readNextStartElement();
                }

                object = new Polygon(markedClasses[className], polygonPoints);
            }
            else if (xmlReader.name() == "st") {
                QPair<int, int> sentenceBeginEnd = readNextXY();
                object = new Sentence(markedClasses[className], sentenceBeginEnd.first, sentenceBeginEnd.second);
            }
            else if (xmlReader.name() == "fr") {
                QString frame = xmlReader.attributes().first().value().toString();
                xmlReader.readNextStartElement();

                QVector<Polygon *> polygons;
                while (xmlReader.name() != "fr") {
                    xmlReader.readNextStartElement();
                    if (xmlReader.name() == "Polygon") {
                        QVector<QPointF> polygonPoints;

                        while (xmlReader.name() != "object") {
                            if (xmlReader.name() == "pt") {
                                QPair<int, int> pointXY = readNextXY();
                                polygonPoints << QPointF(pointXY.first, pointXY.second);
                            }
                            xmlReader.readNextStartElement();
                        }
                        polygons.push_back(new Polygon(markedClasses[className], polygonPoints));
                    }
                }
                object = new Frame(markedClasses[className], frame.toInt(), polygons);
            }

            savedObjects << object;
        }
    }

    return savedObjects;
}

QByteArray Serializer::getData(const QString& filepath)
{
    QFile file(filepath);
    file.open(QIODevice::ReadOnly|QIODevice::Text);
    QByteArray data = file.readAll();
    file.close();

    return data;
}

bool Serializer::write(const QString &filepath, const QVector<MarkedObject*>& objects, OutputType outputType)
{
    if (!objects.isEmpty()) {
        QString document = serialize(objects, outputType);

        if (!document.isEmpty()) {
            QString outputFilename = FileUtils::placeSuffix(filepath, outputType);
            QFile file(outputFilename);
            if (file.open(QIODevice::WriteOnly|QIODevice::Text))
                return file.write(document.toUtf8()) != -1;
        }
    }

    return false;
}
