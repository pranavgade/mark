#include "framecollection.h"

void FrameCollection::clear() {
    clear();
}

QString FrameCollection::unitName() const {
    return "fc";
}

MarkedObject::Type FrameCollection::type() {
    return MarkedObject::Type::Frame; // this isn't used anywhere so should be fine
}


