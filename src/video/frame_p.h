#ifndef FRAME_P_H
#define FRAME_P_H

#include "core/markedobject_p.h"
#include "videopainter.h"


class FramePrivate : public MarkedObjectPrivate
{
public:
    FramePrivate(qint64 frameId, QVector<Polygon*> polygons) : frameId(frameId), polygons(polygons) {}
    FramePrivate(qint64 frameId) : frameId(frameId) {}

    qint64 frameId;
    QVector<Polygon*> polygons;
};


#endif //FRAME_P_H
