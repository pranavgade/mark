#ifndef VIDEOPAINTER_H
#define VIDEOPAINTER_H

#include <QMediaPlayer>
#include <QPushButton>
#include <QSlider>
#include <QBoxLayout>
#include <image/polygon.h>
#include "ui/container.h"
#include "ui/painter.h"
#include "framecollection.h"

class Frame;

class VideoPainter : public Painter
{
public:
    /** Shape of the painter. */
    enum class Shape {
        Polygon,
        Rectangle
    };

    explicit VideoPainter(Container* parent);
    ~VideoPainter();

    void paint(QPoint point, bool isDragging) override;
    void paintObject(MarkedObject* object) override;
    void repaint() override;
    void undo() override;
    void deleteCurrentObject() override;
    void changeItem(const QString& filepath) override;
    bool importObjects(QVector<MarkedObject*> objects) override;
    QVector<MarkedObject*>& savedObjects() override;

private:
    void initLayouts();
    void initConnections();
    void toggle();
    void saveState();
    void loadState();
    void play();
    void pause();
    void next();
    void prev();
    void seek();
    void setPos(qint64 pos);

    Frame* m_currentFrame;
    FrameCollection* m_frameCollection;
    QVector<QGraphicsItem*> m_items;
    QGraphicsItem* m_currentItem;
    QMediaPlayer* m_player;
    QGraphicsScene* m_graphicsScene;
    QGraphicsView* m_graphicsView;
    QBoxLayout* m_controlsLayout;
    QPushButton* m_playButton;
    QPushButton* m_nextButton;
    QPushButton* m_prevButton;
    QPushButton* m_increaseSpeedButton;
    QPushButton* m_decreaseSpeedButton;
    QSlider* m_positionSlider;
    qreal m_scaleW;
    qreal m_scaleH;
    bool m_paused;

    Shape m_shape;
};


#endif //VIDEOPAINTER_H
