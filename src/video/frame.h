#ifndef FRAME_H
#define FRAME_H

#include <memory>
#include <QString>
#include "frame_p.h"
#include "core/markedobject.h"

class Frame : public MarkedObject
{
public:
    Frame(MarkedClass *objClass, qint64 frameId, QVector<Polygon *>& polygons)
            : MarkedObject(std::make_unique<FramePrivate>(frameId, polygons), objClass) {}
    Frame(MarkedClass *objClass, qint64 frameId)
            : MarkedObject(std::make_unique<FramePrivate>(frameId), objClass) {};
    Frame(const Frame& frame)
    : MarkedObject(std::make_unique<FramePrivate>(frame.frameId(), frame.polygons()), frame.objClass()) {}
    Frame(Frame *frame)
    : MarkedObject(std::make_unique<FramePrivate>(frame->frameId(), frame->polygons()), frame->objClass()) {}

    Frame& operator=(Frame &frame);

    void clear() override;
    QString unitName() const override;
    MarkedObject::Type type() override;

    QVector<Polygon*> polygons() const;
    qint64 frameId() const;

    void addPolygon(Polygon *polygon);
    void setFrameId(qint64 frameId);
};


#endif //FRAME_H
