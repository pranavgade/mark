#include "video/videopainter.h"
#include "frame.h"

#include <QGraphicsItem>
#include <QGraphicsVideoItem>
#include <QHBoxLayout>
#include <QSlider>
#include <QStyle>

VideoPainter::VideoPainter(Container* parent) :
        Painter(parent),
        m_shape(Shape::Polygon)
{
    m_parent->setCurrentObject(new Polygon(m_parent->currentObject()->objClass()));
    m_parent->viewport()->installEventFilter(m_parent);
    m_parent->viewport()->setMouseTracking(false);

    initLayouts();
    initConnections();

    m_currentFrame = new Frame(m_parent->currentObject()->objClass(), 0);
    m_frameCollection = new FrameCollection;
}

VideoPainter::~VideoPainter()
{
    for (QGraphicsItem* item : m_items)
        m_parent->scene()->removeItem(item);

    m_items.clear();
    m_frameCollection->clear();

    delete m_playButton;
    delete m_nextButton;
    delete m_prevButton;
    delete m_positionSlider;
    delete m_controlsLayout;
    delete m_player;
    delete m_graphicsScene;
    delete m_graphicsView;
    delete m_parent->layout();
}

void VideoPainter::initLayouts() {
    m_graphicsScene = new QGraphicsScene(m_parent);
    m_graphicsView = new QGraphicsView(m_graphicsScene);
    QGraphicsVideoItem *graphicsVideoItem = new QGraphicsVideoItem;
    m_currentItem = graphicsVideoItem;
    graphicsVideoItem->setSize(QSizeF(640, 480));
    m_graphicsScene->addItem(graphicsVideoItem);
    m_player = new QMediaPlayer;
    m_player->setVideoOutput(graphicsVideoItem);
    m_playButton = new QPushButton;
    m_playButton->setIcon(m_parent->style()->standardIcon(QStyle::SP_MediaPlay));
    m_playButton->setToolTip("Play/Pause");
    m_nextButton = new QPushButton;
    m_nextButton->setIcon(m_parent->style()->standardIcon(QStyle::SP_MediaSeekForward));
    m_nextButton->setToolTip("Next Frame");
    m_prevButton = new QPushButton;
    m_prevButton->setIcon(m_parent->style()->standardIcon(QStyle::SP_MediaSeekBackward));
    m_prevButton->setToolTip("Previous Frame");
    m_positionSlider = new QSlider(Qt::Horizontal);
    m_positionSlider->setRange(0, 100);
    m_increaseSpeedButton = new QPushButton;
    m_increaseSpeedButton->setIcon(m_parent->style()->standardIcon(QStyle::SP_ArrowUp));
    m_increaseSpeedButton->setToolTip("Increase playback speed");
    m_decreaseSpeedButton = new QPushButton;
    m_decreaseSpeedButton->setIcon(m_parent->style()->standardIcon(QStyle::SP_ArrowDown));
    m_decreaseSpeedButton->setToolTip("Increase playback speed");

    m_controlsLayout = new QHBoxLayout;
    m_controlsLayout->setSpacing(5);
    m_controlsLayout->setContentsMargins(5, 0, 5, 5);
    m_controlsLayout->addWidget(m_prevButton);
    m_controlsLayout->addWidget(m_playButton);
    m_controlsLayout->addWidget(m_nextButton);
    m_controlsLayout->addWidget(m_positionSlider);
    m_controlsLayout->addWidget(m_increaseSpeedButton);
    m_controlsLayout->addWidget(m_decreaseSpeedButton);

    QBoxLayout *layout = new QVBoxLayout;
    layout->setMargin(0);
    layout->addWidget(m_graphicsView);
    layout->addLayout(m_controlsLayout);
    m_parent->setLayout(layout);
}

void VideoPainter::initConnections() {
    //loading media is asynchronous, so update slider when loading is done
    QObject::connect(m_player, &QMediaPlayer::durationChanged,
                     [=]() {m_positionSlider->setMaximum(m_player->duration()/(1000/24));});
    //change button text when player is paused/resumed
    QObject::connect(m_player, &QMediaPlayer::stateChanged,
                     [=](QMediaPlayer::State newState) {
                         if (newState == QMediaPlayer::PlayingState)
                             m_playButton->setIcon(m_parent->style()->standardIcon(QStyle::SP_MediaPause));
                         else
                             m_playButton->setIcon(m_parent->style()->standardIcon(QStyle::SP_MediaPlay));
                     });
    //button press handling
    QObject::connect(m_playButton, &QPushButton::released, [=]() {toggle();});
    QObject::connect(m_nextButton, &QPushButton::released, [=]() {next();});
    QObject::connect(m_prevButton, &QPushButton::released, [=]() {prev();});
    QObject::connect(m_increaseSpeedButton, &QPushButton::released, [=]() {m_player->setPlaybackRate(m_player->playbackRate() + 0.1);});
    QObject::connect(m_decreaseSpeedButton, &QPushButton::released, [=]() {m_player->setPlaybackRate(m_player->playbackRate() - 0.1);});

    //slider change handling
    QObject::connect(m_positionSlider, &QSlider::valueChanged, [=]() {seek();});

    //update slider according to player
    QObject::connect(m_player, &QMediaPlayer::positionChanged,
                     [=]() {m_positionSlider->setValue(m_player->position()/(1000/24));});

    //restart when reaches end
    QObject::connect(m_player, &QMediaPlayer::stateChanged,
                     [=](QMediaPlayer::State newState) {
        if (newState == QMediaPlayer::StoppedState) {
            m_playButton->setText("&Replay");
            m_player->setPosition(0);
            m_player->play();
        }
    });
}

void VideoPainter::paint(QPoint point, bool isDragging)
{
    if (!m_paused) return;
    QGraphicsPixmapItem* currentItem = static_cast<QGraphicsPixmapItem*>(m_currentItem);
    if (currentItem != nullptr) {
        QPointF clickedPoint = m_graphicsView->mapToScene(point);

        bool isImageClicked = m_graphicsView->itemAt(point.x(), point.y()) == currentItem;

        Polygon *currentPolygon = static_cast<Polygon *>(m_parent->currentObject());

        if (m_shape == Shape::Polygon) {
            int idxSavedPolygClicked = -1;
            QPointF scaledClickedPoint = QPointF(-m_currentItem->pos());
            scaledClickedPoint += QPointF(scaledClickedPoint.x() / m_scaleW, scaledClickedPoint.y() / m_scaleH);
            for (int i = 0; i < m_currentFrame->polygons().size(); i++) {
                const Polygon *polygon = (m_currentFrame->polygons()[i]);
                if (polygon->containsPoint(scaledClickedPoint, Qt::OddEvenFill)) {
                    idxSavedPolygClicked = i;
                    break;
                }
            }

            bool isSavedPolygClicked = idxSavedPolygClicked != -1;
            if (isSavedPolygClicked) {
                if (isDragging)
                    return;
                delete currentPolygon;
                m_parent->setCurrentObject(m_currentFrame->polygons()[idxSavedPolygClicked]);
                m_currentFrame->polygons().remove(idxSavedPolygClicked);
                currentPolygon = static_cast<Polygon *>(m_parent->currentObject());
                currentPolygon->unscale(m_currentItem->pos(), m_scaleW, m_scaleH);
                currentPolygon->pop_back();
            }

            bool isPolygFirstPtClicked = false;
            if (!currentPolygon->empty()) {
                QPointF cPolygFirstPt = currentPolygon->first();
                QRectF cPolygFirstPtRect(cPolygFirstPt, QPointF(cPolygFirstPt.x() + 10, cPolygFirstPt.y() + 10));
                isPolygFirstPtClicked = cPolygFirstPtRect.contains(clickedPoint);
                if (isPolygFirstPtClicked)
                    clickedPoint = cPolygFirstPt;
            }

            if (isSavedPolygClicked || isPolygFirstPtClicked || isImageClicked) {
                *currentPolygon << clickedPoint;

                if (currentPolygon->size() > 1 && currentPolygon->isClosed()) {
                    currentPolygon->scale(m_currentItem->pos(), m_scaleW, m_scaleH);
                    m_currentFrame->addPolygon(new Polygon(currentPolygon));
                    m_parent->appendObject(currentPolygon);
                    m_parent->setCurrentObject(new Polygon(currentPolygon->objClass()));
                }

                repaint();
            }
        } else if (m_shape == Shape::Rectangle) {
            bool toSave = point.isNull() && currentPolygon->size() > 1;
            if (isImageClicked) {
                if (currentPolygon->empty())
                    *currentPolygon << clickedPoint;

                else {
                    QPointF firstPt = currentPolygon->first();
                    if (isDragging) {
                        currentPolygon->clear();
                        *currentPolygon << firstPt;
                    } else
                        toSave = true;
                    *currentPolygon << QPointF(clickedPoint.x(), firstPt.y()) << clickedPoint
                                    << QPointF(firstPt.x(), clickedPoint.y()) << firstPt;
                }

                repaint();
            }
            if (toSave) {
                currentPolygon->scale(m_currentItem->pos(), m_scaleW, m_scaleH);
                m_parent->appendObject(currentPolygon);
                m_parent->setCurrentObject(new Polygon(m_parent->currentObject()->objClass()));
            }
        }
    }

}

void VideoPainter::changeItem(const QString& filepath)
{
    m_player->pause();
    m_player->setMedia(QUrl::fromLocalFile(filepath));
    m_player->setPosition(0);
    m_paused = true;

    m_positionSlider->setMinimum(0);

    m_scaleW = 1.0;
    m_scaleH = 1.0;

    m_currentFrame = new Frame(m_parent->currentObject()->objClass(), 0);
}

void VideoPainter::repaint()
{
    for (QGraphicsItem* item : m_items)
        m_graphicsScene->removeItem(item);

    m_items.clear();

    for (MarkedObject* obj : m_currentFrame->polygons())
        paintObject(obj);

    paintObject(m_parent->currentObject());
}

void VideoPainter::undo()
{
    Polygon* polygon = static_cast<Polygon*>(m_parent->currentObject());

    if (!polygon->empty()) {
        polygon->pop_back();
        repaint();
    }
}

void VideoPainter::deleteCurrentObject()
{
    Polygon* polygon = static_cast<Polygon*>(m_parent->currentObject());

    if (!polygon->isClosed()) {
        polygon->clear();
        repaint();
    }
}

void VideoPainter::paintObject(MarkedObject* object)
{
    Polygon polygon = dynamic_cast<Polygon*>(object);
    if (object != m_parent->currentObject()) {
        QPointF offset = m_currentItem->pos();
        for (QPointF& point : polygon) {
            point = QPointF(point.x() * m_scaleW, point.y() * m_scaleH);
            point += offset;
        }
    }

    QColor color(polygon.objClass()->color());
    QBrush brush(color);
    QPen pen(brush, 2);

    if (polygon.size() > 1 && polygon.isClosed()) {
        color.setAlpha(35);

        QGraphicsPolygonItem* pol = m_graphicsScene->addPolygon(polygon, pen, QBrush(color));

        m_items << pol;
    }
    else {
        for (auto it = polygon.begin(); it != polygon.end(); ++it) {
            QGraphicsItem* item;
            if (it == polygon.begin())
                item = m_graphicsScene->addRect((*it).x(), (*it).y(), 10, 10, pen, brush);
            else
                item = m_graphicsScene->addLine(QLineF(*(it - 1), *it), pen);

            m_items << item;
        }
    }
}

bool VideoPainter::importObjects(QVector<MarkedObject*> objects)
{
    if (objects.isEmpty()) return false;
    FrameCollection* data;
    for (auto obj : objects) {
        if (obj->type() != MarkedObject::Type::Frame) return false;

        Frame* frame = dynamic_cast<Frame *>(obj);
        data->insert(frame->frameId(), frame);
    }
    m_frameCollection->clear(); // clear only if importing was successful
    m_frameCollection = data;
//    return false; // true causes a SIGABRT, why?
    return true;
}

QVector<MarkedObject *> &VideoPainter::savedObjects() {
    m_currentFrame->setFrameId(m_player->position() / (1000 / 24));
    m_frameCollection->insert(m_player->position() / (1000 / 24), m_currentFrame);
    static QVector<MarkedObject *> ret;

    for (auto it = m_frameCollection->begin(); it != m_frameCollection->end(); it++) {
        ret << static_cast<MarkedObject*>(it.value());
    }

    return ret;
}

void VideoPainter::toggle() {
    if (m_paused)
        play();
    else
        pause();
}

void VideoPainter::saveState() {
    m_currentFrame->setFrameId(m_player->position() / (1000 / 24));
    m_frameCollection->insert(m_player->position() / (1000 / 24), m_currentFrame);
    m_currentFrame = new Frame(m_parent->currentObject()->objClass(), -1);

    m_parent->reset();
}

void VideoPainter::loadState() {
    // TODO this is called twice by seek() in some cases. This might be inefficient,
    //  but might just be how slider handles signals
    if (m_frameCollection->contains(m_player->position() / (1000 / 24))) {
        m_currentFrame = m_frameCollection->take(m_player->position() / (1000 / 24));

        for (Polygon* polygon : m_currentFrame->polygons()) {
            m_parent->appendObject(polygon);
        }
        m_parent->setCurrentObject(new Polygon(m_parent->currentObject()->objClass()));
    }
    repaint();
}

void VideoPainter::play() {
    saveState();

    m_player->play();
    m_paused = false;
}

void VideoPainter::pause() {
    m_player->pause();
    m_paused = true;

    loadState();
}

void VideoPainter::next() {
    setPos(m_player->position() + (1000/24));
}

void VideoPainter::prev() {
    setPos(m_player->position() - (1000/24));
}

void VideoPainter::seek() {
    setPos(m_positionSlider->value() * (1000/24));
}

void VideoPainter::setPos(qint64 pos) {
    if (m_paused)
        saveState();

    m_player->setPosition(pos);

    if (m_paused)
        loadState();
}
