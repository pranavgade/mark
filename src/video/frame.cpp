#include "frame.h"

QString Frame::unitName() const
{
    return "fr";
}

MarkedObject::Type Frame::type()
{
    return MarkedObject::Type::Frame;
}

void Frame::clear() {
    (static_cast<FramePrivate*>(d_p.get()))->polygons.clear();
}

QVector<Polygon *> Frame::polygons() const {
    return (static_cast<FramePrivate*>(d_p.get()))->polygons;
}

qint64 Frame::frameId() const {
    return (static_cast<FramePrivate*>(d_p.get()))->frameId;
}

Frame &Frame::operator=(Frame &frame) {
    (static_cast<FramePrivate*>(d_p.get()))->m_objClass = frame.objClass();
    (static_cast<FramePrivate*>(d_p.get()))->frameId = frame.frameId();
    (static_cast<FramePrivate*>(d_p.get()))->polygons = frame.polygons();
    return *this;
}

void Frame::addPolygon(Polygon *polygon) {
    (static_cast<FramePrivate*>(d_p.get()))->polygons << polygon;
}

void Frame::setFrameId(qint64 frameId) {
    (static_cast<FramePrivate*>(d_p.get()))->frameId = frameId;
}
