#ifndef FRAMECOLLECTION_H
#define FRAMECOLLECTION_H

#include "core/markedobject.h"

class Frame;

class FrameCollection : public MarkedObject, public QMap<qint64, Frame*>
{
public:
    // since each frame might have different object class, we don't care about it
    FrameCollection() : MarkedObject(nullptr) {};

    void clear() override;
    QString unitName() const override;
    MarkedObject::Type type() override;

};


#endif //FRAMECOLLECTION_H
